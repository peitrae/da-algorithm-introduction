function isLampButton(n) {
    let lamp = 0;

    for (let i = 1; i <= n; i++) {
        if (n % i === 0) {
            lamp = !lamp
        }
    }

    return lamp ? 'lampu nyala' : 'lampu mati';
}